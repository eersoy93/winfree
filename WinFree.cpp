﻿// Copyright(c) 2023 Erdem Ersoy (eersoy93)
//
// Licensed with MIT license. See LICENSE file for full text.

#include <windows.h>
#include <iostream>

int main(void)
{
    MEMORYSTATUSEX statex;

    statex.dwLength = sizeof(statex);

    GlobalMemoryStatusEx(&statex);

    DWORDLONG totalPhysicalMemory = statex.ullTotalPhys / (DWORDLONG)1048576;
    DWORDLONG freePhysicalMemory = statex.ullAvailPhys / (DWORDLONG)1048576;
    DWORDLONG usedPhysicalMemory = totalPhysicalMemory - freePhysicalMemory;

    std::cout << "Total physical memory: " << totalPhysicalMemory << " MB" << std::endl;
    std::cout << "Free physical memory: " << freePhysicalMemory << " MB" << std::endl;
    std::cout << "Used physical memory: " << usedPhysicalMemory << " MB" << std::endl;

    return EXIT_SUCCESS;
}
